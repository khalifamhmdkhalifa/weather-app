//
//  HomeViewModel.swift
//  WeatherApp
//
//  Created by khalifa on 1/23/21.
//

import Foundation

protocol HomeViewModel: ObservableObject {
    var weatherStateDescription: String? { get }
    var tepreatureDescription: String? { get }
    var weatherState: WeatherState? { get }
    var city: String? { get }
    var errorMessage: String? {get set}
    var loading: Bool { get }
    func loadWeatherInfo()
}

class HomeViewModelImpl: HomeViewModel {
    @Published var weatherStateDescription: String?
    @Published var tepreatureDescription: String?
    @Published var city: String?
    @Published var errorMessage: String?
    @Published var loading: Bool = false
    @Published var weatherState: WeatherState?
    private var temperature: Double? {
        didSet {
            if let temperature = self.temperature {
                let mf = MeasurementFormatter()
                mf.numberFormatter.maximumFractionDigits = 0
                mf.unitOptions = .providedUnit
                var ms = Measurement(value: temperature, unit: UnitTemperature.kelvin)
                ms.convert(to: .celsius)
                tepreatureDescription = mf.string(from: ms)
            } else {
                tepreatureDescription = ""
            }
        }
    }
    private var locationManager: LocationManager
    private var interactor: HomeInteractor?
    
    init(locationManager: LocationManager, interactor: HomeInteractor) {
        self.interactor = interactor
        self.locationManager = locationManager
    }
    
    func loadWeatherInfo() {
        locationManager.getUserLocation()
    }
    
    private func udpateInfo(model: WeatherInfoModel) {
        self.temperature = model.current?.temp ?? 0
        weatherState = model.current?.weather?.first?.main
        self.weatherStateDescription = model.current?.weather?.reduce(String(), {
            if let desc = $1.description {
                return $0 + ($0.isEmpty ? "" : ",") + desc
            } else { return $0 }
        })
    }
    
    private func loadWeatherInfo(lat: Double, long: Double) {
        loading = true
        interactor?.loadWeatherInfo(lat: lat, lon: long, completion: { [weak self] (result, errorMessage) in
            guard let self = self else { return }
            self.loading = false
            if let result = result {
                self.udpateInfo(model: result)
            } else if let error = errorMessage {
                self.errorMessage = error
            }
        })
    }
}

extension HomeViewModelImpl: LocationManagerDelegate {
    func onLocationLoaded(lat: Double, long: Double) {
        locationManager.getCityName(lat: lat, long: long, completion: { self.city = $0 })
        loadWeatherInfo(lat: lat, long: long)
    }
    
    func onError(_ error: String) {
        self.errorMessage = error
    }
    
    func onPermissionNeeded() {
        locationManager.askUserForLocationPermission()
    }
    
    func onPermissionDenied() {
        self.errorMessage = "App Will Not be able To Retrive Weather Info unless you Allow it to get Your location, Please Allow The App to access your Location And Make sure Location service is Enabled"
    }
    
    func onPermissionGiven() {
        locationManager.getUserLocation()
    }
}
