//
//  HomeInteractor.swift
//  WeatherApp
//
//  Created by khalifa on 1/23/21.
//

import Foundation

protocol HomeInteractor {
    func loadWeatherInfo(lat: Double, lon: Double, completion: @escaping (WeatherInfoModel?, String?) -> Void)
}

class HomeInteractorImpl: HomeInteractor {
    private static let weatherInfoUrl = "/onecall"
    private var coreNetwork: CoreNetworkProtocol
    
    init(coreNetwork: CoreNetworkProtocol) {
        self.coreNetwork = coreNetwork
    }
    
    func loadWeatherInfo(lat: Double, lon: Double, completion: @escaping (WeatherInfoModel?, String?) -> Void) {
        var parameters: [String: AnyObject] = [:]
        parameters["lat"] = lat as AnyObject
        parameters["lon"] = lon as AnyObject?
        let request = RequestSpecs<WeatherInfoModel>(method: .GET, urlString: Self.weatherInfoUrl, parameters: parameters)
        coreNetwork.makeRequest(request: request) { (result, error) in
            completion(result, error?.localizedDescription)
        }
    }
}
