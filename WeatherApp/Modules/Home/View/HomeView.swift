//
//  HomeView.swift
//  WeatherApp
//
//  Created by khalifa on 1/23/21.
//

import SwiftUI

struct HomeView<Model: HomeViewModel>: View {
    @ObservedObject var model: Model
    @State var isAlertPresented: Bool = false
    private var icon: String {
        guard let state = model.weatherState else { return ""}
        switch state {
        case .thunderstorm: return "cloud.bolt.rain.fill"
        case .drizzle : return "cloud.drizzle.fill"
        case .snow: return "cloud.snow.fill"
        case .mist: return "cloud.fog.fill"
        case .smoke, .haze, .dust: return "smoke.fill"
        case .fog, .sand, .ash, .squall,.tornado : return "cloud.fog.fill"
        case .clear: return "sun.stars.fill"
        case .clouds: return "cloud.fill"
        case .rain: return "cloud.heavyrain.fill"
        }
    }
    private var background: some View {
        guard let state = model.weatherState  else { return LinearGradient(gradient: Gradient(colors: [Color.white, Color.gray]), startPoint: .top, endPoint: .bottom) }
        switch state {
        case .clear:
            return LinearGradient(gradient: Gradient(colors: [Color( #colorLiteral(red: 0.9764705896, green: 0.850980401, blue: 0.5490196347, alpha: 1)), Color(#colorLiteral(red: 0.9529411793, green: 0.8685067713, blue: 0.1800223484, alpha: 1))]), startPoint: .top, endPoint: .bottom)
        case .rain,.snow,.thunderstorm,.tornado, .drizzle:
            return LinearGradient(gradient: Gradient(colors: [Color(#colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)), Color(#colorLiteral(red: 0.1596036421, green: 0, blue: 0.5802268401, alpha: 1))]), startPoint: .top, endPoint: .bottom)
        case .ash,.clouds,.dust,.fog,.haze,.mist,.sand,.squall, .smoke:
            return LinearGradient(gradient: Gradient(colors: [Color( #colorLiteral(red: 0.6324083141, green: 0.8039215803, blue: 0.7850640474, alpha: 1)), Color( #colorLiteral(red: 0.4545597353, green: 0.393878495, blue: 0.5369011739, alpha: 1))]), startPoint: .top, endPoint: .bottom)
        }
    }
    
    var body: some View {
        let alertBool = Binding<Bool>(
            get: { self.model.errorMessage != nil },
            set: { _ in self.model.errorMessage = nil }
        )
        ZStack {
            background
            if !model.loading {
                weatherInfoView
            } else {
                ProgressView()
            }
        }.edgesIgnoringSafeArea(.all)
        .onAppear() {
            self.model.loadWeatherInfo()
        }.alert(isPresented: alertBool) {
            Alert(title: Text(""), message: Text(self.model.errorMessage ?? ""), dismissButton: .default(Text("Ok")))
        }
    }
    
    var weatherInfoView: some View {
        VStack {
            HStack {
                Image(systemName: "mappin.circle")
                    .foregroundColor( Color(.white))
                Text(self.model.city ?? "--")
                    .foregroundColor(Color.white)
                    .font(.system(size: 25))
                    .fontWeight(.bold)
            }
            .padding([.leading,.trailing], 16)
            Text("\(self.model.weatherStateDescription ?? "--")")
                .fontWeight(.bold)
                .foregroundColor(Color.white)
                .font(.system(size: 25)).padding(.bottom, 30)
                .padding([.leading,.trailing], 16)
                .lineLimit(3)
                .frame(maxWidth: .infinity).padding([.leading,.trailing], 16)
            if model.weatherState != nil, let image = Image(systemName: icon) {
                image.foregroundColor(Color.white)
            }
            Text("\(self.model.tepreatureDescription ?? "--")")
                .foregroundColor(Color.white)
                .font(.system(size: 25))
                .fontWeight(.bold)
                .padding([.leading,.trailing], 16)
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        return HomeView<HomeViewModelImpl>.getConfiguredInstance()
    }
}

extension HomeView where Model == HomeViewModelImpl {
    static func getConfiguredInstance() -> HomeView<Model> {
        let locationManager = LocationManagerImpl(delegate: nil)
        let viewModel = HomeViewModelImpl(locationManager: locationManager, interactor: HomeInteractorImpl(coreNetwork: CoreNetwork.sharedInstance))
        locationManager.delegate = viewModel
        return HomeView<HomeViewModelImpl>(model: viewModel)
    }
}

