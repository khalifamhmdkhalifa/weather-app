//
//  WeatherInfoModel.swift
//  WeatherApp
//
//  Created by khalifa on 1/23/21.
//

import Foundation

struct WeatherInfoModel: Codable {
    let lat, lon: Double?
    let current: WeatherStateInfo?
    
    enum CodingKeys: String, CodingKey {
        case lat, lon
        case current
    }
}

struct WeatherStateInfo: Codable {
    let temp, feelsLike: Double?
    let pressure, humidity: Int?
    let weather: [Weather]?
    
    enum CodingKeys: String, CodingKey {
        case temp, feelsLike
        case pressure, humidity
        case weather
    }
}

struct Weather: Codable {
    let id: Int?
    let main: WeatherState?
    let description: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case main
        case description
    }
}

struct FeelsLike: Codable {
    let day, night, eve, morn: Double?
}

struct Temp: Codable {
    let day, min, max, night: Double?
    let eve, morn: Double?
}


enum WeatherState: String, Codable {
    case thunderstorm = "Thunderstorm"
    case drizzle = "Drizzle"
    case rain = "Rain"
    case snow = "Snow"
    case mist = "Mist"
    case smoke = "Smoke"
    case haze = "Haze"
    case dust = "Dust"
    case fog = "Fog"
    case sand = "Sand"
    case ash =  "Ash"
    case squall = "Squall"
    case tornado = "Tornado"
    case clear = "Clear"
    case clouds = "Clouds"
}
